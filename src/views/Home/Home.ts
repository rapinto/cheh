import { Vue, Options } from 'vue-class-component';
import axios from 'axios';

export const axiosClient = axios.create({
  baseURL: 'https://api.binance.com',
  withCredentials: false,
  headers: {},
  timeout: 20000,
});

@Options({
  name: 'Home',
  template: require('./Home.html'),
})
export default class Home extends Vue {
  protected amount = 0.01083;
  protected value: number | null = null;

  public created(): void {
    void this.btcEuroValue();
  }

  private async btcEuroValue(): Promise<void> {
    await axiosClient.get('/api/v3/ticker/price').then((resp: any) => {
      console.log(resp.data);
      console.log('type', typeof resp.data);
      const result = resp.data.filter((item: any) => item.symbol === 'BTCEUR');
      console.log(result);
      const val = result[0];
      this.value = val.price * this.amount;
      console.log('price', this.value);
    });
  }
}
